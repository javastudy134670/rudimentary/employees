/* Copyright � 2015 Oracle and/or its affiliates. All rights reserved. */
package com.example.employees;

import org.apache.catalina.startup.Tomcat;

public class Main {

    public static final String PORT = System.getenv("PORT");
    public static final String HOSTNAME = System.getenv("HOSTNAME");

    public static void main(String[] args) throws Exception {
        String contextPath = "/";
        String appBase = ".";
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(Integer.parseInt((PORT == null) ? "8080" : PORT));
        tomcat.setHostname((HOSTNAME == null) ? "localhost" : HOSTNAME);
        tomcat.getHost().setAppBase(appBase);
        tomcat.addWebapp(contextPath, appBase);
        tomcat.start();
        tomcat.getServer().await();
    }
}